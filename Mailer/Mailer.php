<?php
namespace UsersBundle\Mailer;

use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use FOS\UserBundle\Mailer\Mailer as BaseMailer;

class Mailer extends BaseMailer
{
    /**
     * @param UserInterface $user
     */
    public function sendWelcomeEmailMessage(UserInterface $user)
    {
        /**
         * Custom template using same positioning as
         * FOSUserBundle:Registration:email.txt.twig so that the sendEmailMessage
         * method will break it up correctly
         */

        $template = 'UsersBundle:Emails:welcome.html.twig';
        $url = $this->router->generate('UsersBundle_welcome', array('token' => $user->getConfirmationToken()), UrlGeneratorInterface::ABSOLUTE_URL);
        $rendered = $this->templating->render($template, array(
            'user' => $user,
            'confirmationUrl' => $url,
        ));
        $this->sendEmailMessage($rendered, $this->parameters['from_email']['resetting'], (string) $user->getEmail());
    }
}