<?php
namespace UsersBundle\Form\Type;

use FOS\UserBundle\Util\LegacyFormHelper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegistrationFormType extends AbstractType
{
    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';

        // Or for Symfony < 2.8
        // return 'fos_user_registration';
    }

    public function getBlockPrefix()
    {
        return 'app_user_registration';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        //==
        $builder->remove('plainPassword');
        $builder->add('afiliado', EntityType::class, array(
            "class"=>'AppBundle\Entity\Afiliado',
            "required" => true,
            'placeholder' => 'Elije un afiliado'
        ));
        $builder->add(
            'roles', ChoiceType::class, [
                'choices' => array(
                    "Administrador"=>"ROLE_ADMIN",
                    "Finanzas"=>"ROLE_FINANZAS",
                    "Secretaría" => "ROLE_SECRETARIA",
                    "Propaganda" => "ROLE_PROPAGANDA",
                    "Corte electoral" => "ROLE_ELECTORAL",
                    "Delegado sectorial" => "ROLE_SECTOR"
                ),
                'expanded' => true,
                'multiple' => true,
                'label' => 'Roles especiales',
                'choices_as_values' => true,
                'choice_attr' => array(
                    'Administrador' => array('hint'=>'Tiene acceso a todas las opciones del sistema.')
                )
            ]
        );
    }
}
