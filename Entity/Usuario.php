<?php
namespace UsersBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="usuarios")
 */
class Usuario extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Afiliado", inversedBy="usuario", cascade={"all"})
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    protected $afiliado;


    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Set afiliado
     *
     * @param \AppBundle\Entity\Afiliado $afiliado
     *
     * @return Usuario
     */
    public function setAfiliado(\AppBundle\Entity\Afiliado $afiliado = null)
    {
        $this->afiliado = $afiliado;

        return $this;
    }

    /**
     * Get afiliado
     *
     * @return \AppBundle\Entity\Afiliado
     */
    public function getAfiliado()
    {
        return $this->afiliado;
    }
}
