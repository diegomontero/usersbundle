<?php

namespace UsersBundle\Controller;

use UsersBundle\Entity\Usuario;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class UserController extends Controller
{
    /**
     * @Route("/usuario/{usuario}", name="usuario_show", options={"expose"=true})
     */
    public function verAction(Usuario $usuario)
    {
        return null;
    }

    /**
     * @Route("/usuario/editar/{usuario}", name="usuario_edit", options={"expose"=true})
     */
    public function editarAction(Usuario $usuario)
    {
        return null;
    }

    /**
     * @Route("/usuarios", name="usuarios")
     * @Method("GET")
     */
    public function indexAction()
    {
        $datatable = $this->get('app.datatable.usuarios');
        $datatable->buildDatatable();

        return $this->render('usuarios/index.html.twig', array(
            'datatable' => $datatable,
        ));
    }

    /**
     * @Route("/usuarios/results", name="usuarios_results")
     */
    public function indexResultsAction()
    {
        $datatable = $this->get('app.datatable.usuarios');
        $datatable->buildDatatable();

        $query = $this->get('sg_datatables.query')->getQueryFrom($datatable);

        return $query->getResponse();
    }
}
